```mermaid
classDiagram
    user "1" --> "1..*" favorite : One to Many
    nft "1" --> "1..*" favorite : One to Many    
    nft "1" --> "1..*" changelog : One to Many


    class user {
        serial id PRIMARY KEY
    }

    class nft {
        bigserial id PRIMARY KEY
        varchar[42] address 
        bigint token_id
        varchar token_uri
        varchar[42] owner_address
        varchar[24] name
        UNIQUE(address, token_id)
        CREATE INDEX ON nft ((lower(name)))
        CREATE INDEX ON nft (owner_address)
    }

    class favorite {
        serial id PRIMARY KEY
        bigserial nft_id REFERENCES nft ON DELETE CASCADE
        serial user_id REFERENCES user ON DELETE CASCADE
        timestamptz date_favorited
        UNIQUE(nft_id, user_id)
        CREATE INDEX ON favorite (DATE(date_favorited AT TIME ZONE 'UTC'));
    }

    class changelog {
        bigserial id PRIMARY KEY
        jsonb old
        jsonb new
        bigserial nft_id REFERENCES nft ON DELETE CASCADE
        timestamptz timestamp
    }
```

## Notes:
* postgresql flavor of sql db is assumed
* bigint is used for token_id due to assumption that token_id's are only positive. Even though integer fields are 4bit they are unsigned and therefore would not have enough space
* bigserial may not be necessary for nft's and changelog's primary key. I would need a greater understanding of the number of NFTs, growth rate, and the rate of changes. I aired on the side of caution and used bigserial(8bytes) over serial(4 bytes). This does increase nft record size and the size of records that have a foreign key to the nft table.
* id is used as the primary key for nft instead of a composite key (address, token_id). Composite keys are not supported by multiple ORMs (see Django ORM). Taking on the increased nft record size and cognitive overhead for the added flexibility and convenience seems like a worthwhile tradeoff.
* numeric ids are used as they are more performant in queries and space efficient, but if we wish to use these ids across multiple systems this is something we will want to rethink.
