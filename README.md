# [Backend Eng] NFT Takehome Challenge
 
## [Part 1] Design an API
[API documentation](https://sus-nft.stoplight.io/docs/trm-nft/YXBpOjI3OTA1MTgw-backend-eng-nft-takehome-challenge)
 
 
## [Part 2] Design your system
> **_NOTE:_**  failure modes are discussed in `NFT updating architecture`
 
[DB schema](db_schema.md)
 
[NFT updating architecture](nft_updating.md)
 
[Sequence Diagrams for User Stories](sequence_diagrams.md)
 
## [Part 3] Future Improvements
### Changes to User Stories but no requirement changes:
* More granular profiling of usage requirements. Granted, these were likely intentionally left vague and something that I could have likely asked about. This information would allow me to more confidently make tradeoff decisions. For example:
> will a user's favorite list grow to a size that would necessitate pagination? If not, could we consider client side sorting and filtering for a user's favorites? This could greatly reduce RDS load.
> instead of a general 100 req/sec give granular expected req/sec for each given endpoint.
 
* An understanding of expected future requirements. Will we want to support multiple clients? Will we want to prioritize reducing staleness of NFT data? Will we want to expand search performance and/or capabilities? Will we want to expand NFT metadata ingested? Again, this would be useful in making tradeoff decisions.
 
In lieu of having this information I have opted to prioritize extensibility and simplicity. I tried to avoid over optimization (query tuning, caching) and introduced read/write replicas. Read replicas will help us make the system more resilient, performant, and allows us to make schema changes and system changes with reduced site impact.
 
### New Requirements:
* allow the user to refresh an NFT's data. This can be accomplished by creating a `priority_queue` in the Redis instance. When a user requests a refresh an API call would be made to the server to enqueue the selected NFT into the `priority_queue`. There would be a discrete set of workers watching the `priority_queue`. Additionally, I would like to add a socket connection between the client and server allowing the user to get closer to real time data about the state of the sync without having to rely on polling. This would allow users to not wait until the next full sync in order to see up to date data.
 
* Fuzzy NFT searching on NFT name. Depending on the specifics this would likely require a new piece of technology. I am most familiar with ElasticSearch. Introducing ElasticSearch would complicate the architecture and greatly increase the cost to update NFT data as we would need to reindex the ElasticSearch instance on each modification.
 
* Ingesting New NFTs. Currently, my architecture does not support ingesting new NFTs. It would be an interesting and seemingly useful requirement to have as a bonus requirement at the end. With my proposed architecture, we could simply leverage a similar pattern to updating NFT data assuming there is an external API that can give us the necessary data.
 
* uint256 tokenId for NFT. uint256 is the size of an NFTs tokenId on Ethereum mainnet. uint256 tokenId instead int32 would feel more real. No other reason why I would want this. I understand why this was not asked for this challenge as that size poses interesting challenges to search performance that I would need more time to work through.
 
### System Design
* Test! If I had more time/resources I would have liked to create this infrastructure, seed the database and perform a test with simulated API requests. This would allow me to create a rudimentary profile for the system and tune resource allocation (number of instances, instance resources). I would make sure to run these tests twice with and without NFT syncs running.
 
* Logging, Monitoring, Alerting strategy. In order for me to be able to improve and scale the architecture we would want to be able to have system metrics, report on issues, and log system events. This would allow us to debug issues, respond to failure modes, and make informed architectural decisions.
 
* document style datastore for NFT metadata. I could easily imagine a future where the requirements of this system change to include richer, less structured NFT metadata. If that were the case my RDS approach to storing the NFT metadata would become a problem due the cumbersome, opinionated RDS schema. Additionally, RDS is not well suited to store and scan large column values or a table with a large number of fields. Performance would tank in either scenario. This would also remove the need for expensive joins while searching NFTs, and would allow for more powerful changelog design, and likely remove the need cached `last_refreshed_at` for each NFT.
 
* specialized search datastore for NFT metadata. RDS is not well suited for scalable search when the data is unstructured, has large values for attributes, or contains a large number of attributes. Granted, I believe with the current constraints (metadata size, # of metadata fields, lack of fuzzy search) leveraging an RDS for search would suffice.
 
* Caching. I largely tried to avoid caching as I believe that without a system profile and strong understanding of system usage patterns caching is too eager of an optimization. With more time and resources, caching is something I would like to investigate further. Given the semi-static nature of the nft data and changelog records, I do think caching could be leveraged. Caching would not only reduce response times but it would also reduce overall load on the system. Granted, there would be an increase in maintenance overhead having to manage cache state.
 
* API spec/language. This would increase velocity for engineers working in the system and engineers consuming the API by reducing cognitive overhead and maintenance surface area. Depending on our technology choice and our api usage patterns, the system could also experience performance benefits. Graphql for example could reduce the number of round trips necessary to fetch the information for a given page.
 
* Event stream. An event stream connection (socket) between the client and the server could allow for the user to receive updated information without a refresh and without expensive polling. This would be useful for the user in regards to NFT staleness, particularly if we allow the user to manually refresh NFTs.
 
* Workflow management tool (e.g. Airflow). A workflow management tool like Airflow would be beneficial to effectively manage the system's workflows at scale. It would allow us to create more resilient and observable workflows by creating workflow dags. Currently, the system has one workflow (update nfts), but I could easily see the number of workflows expanding (new nft ingestion). This would also help if we ever wanted analytic ETL workflows.
