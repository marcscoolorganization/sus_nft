```mermaid
flowchart
    server([Server Function])
    cron([Cron Job])
    psql_write[(psql write - RDS)]
    psql_readN[(psql read N - RDS)]
    workerN([Worker N])
    redis[(Redis - NoSQL)]
    NFTAPI(NFT ETH API)

    cron -->|1. run server function on schedule|server
    server -->|2. query nft records|psql_readN
    psql_readN -->|3. return nft records|server
    server -->|4. enqueue nft_id |redis
    workerN -->|5. pop nft_id |redis
    redis --> |6. return popped nft_id |workerN
    workerN --> |7. request NFT data|NFTAPI
    NFTAPI --> |8. return NFT data|workerN
    workerN --> |9. determine if change has occured|workerN
    workerN --> |10. transaction to create changelog and update nft if change occured|psql_write
    psql_write --> |11. transaction response|workerN
    workerN --> |12. update last_refreshed_at for NFT in redis regardless of step 9|redis
    psql_write --> |replication|psql_readN
```
## Notes:
* for `worker N` and `psql read N - RDS` the `N` implies that there can be any number of instances running that service. This number should be tuned after profiling is done
* read/write replicas are used as inserting into NFT will be longer than standard inserts and a blocking transaction due to indexes. Additionally, it gives us more scalability (more read instances), resilience (promote reads), and extensibility (reduced site impact of schema changes and system changes).
 
## Step Details:
* 1. The cron schedule initially could be set to 12 hours and tuned based on performance and business needs. Failure mode cron job is not properly registered and fails to run at the desired schedule. This failure mode could be addressed with monitoring alerting team members if cronjob didn't run on its specified schedule.
* 2. failure mode query fails due to timeout. This could be addressed a number of different ways but a fix that would require minimal system changes would be to maintain a list of nft_ids in redis. As we are not currently concerned with adding new nft's the dataset could be considered static and can be maintained manually or via different mechanisms. Instead of querying nft records and adding them to the queue in redis would could simply duplicate the set and change one set to have the live queue key.
* 7. failure mode: NFT API rate limiting! this is could be addressed with backoff, timeouts, and retries
* 8. failure mode: NFT API datashape change. There is no silver bullet for this. Monitoring and alerting would help us address the issue quickly.
* 9. this step could be performed in a number of ways. One performant approach would be to have the nft's metadata hash in Redis. The downside of this approach is state management and redis memory bloat. Another approach with no statement management but more queries on our read replicas would be to requery the nft from a read replica then perform an in-memory value check. After system profiling, I would be able to properly assess the tradeoffs of each approach.
* 10. failure mode: inconsistent changelog, nft state that could occur when either one of the transactions fails. Address this by placing both the insert and update into one db transaction.
* 11. each NFT will have k,v in redis where the key is the NFTs id and the value will be utc.now(). When clients fetch nft data this approach allows us to efficiently determine the last_refreshed_at time for each NFT without bloating the nft table and reducing insert speeds.
* replication lag! This can be caused by a number of issues ranging from issues with the server boxes themselves to query performance. I would address this by first analyzing long running queries and queries that were running at the time of lag.
