# Sequence Diagrams
## Favorite NFT
```mermaid
sequenceDiagram
    actor User
    participant C as Client
    participant A as API
    participant R as RDS
    User->>C: favorites NFT
    C->>A: POST to /users/{userId}/favorites
    A->>R: creates new record in favorites table
    R->>A: success response
    A->>C: respond with 200 and newly created favorite
    C->>User: visual que noticed
```

## Unfavorite NFT
```mermaid
sequenceDiagram
    actor User
    participant C as Client
    participant A as API
    participant R as RDS
    User->>C: unfavorites NFT
    C->>A: DELETE /users/{userId}/favorites/{favoriteId}
    A->>R: deletes record in favorites table
    R->>A: success response
    A->>C: respond 200
    C->>User: visual que noticed
```

## Search NFT
NOTE: to avoid writing to the nft table ever sync regardless of change or not, the update_nft process instead has workers set a refresh datetime in redis for each nft
```mermaid
    sequenceDiagram
        actor User
        participant C as Client
        participant A as API
        participant R as RDS
        participant Redis
        User->>C: submit search criteria
        C->>A: GET /nfts/{nftId}?address={address}&tokenId={tokenId}
        A->>R: queries nft table for all records with the given address and token_id
        R->>A: return matching favorite records
        A->>Redis: request last_synced_at time for each nft
        Redis->>A: return last_synced_at time for each nft
        A->>C: respond with 200 and array of favorite records
        C->>User: display search results
```

## Filter and Sort Favorites
```mermaid
sequenceDiagram
    actor User
    participant C as Client
    participant A as API
    participant R as RDS
    User->>C: submit sort criteria
    C->>A: GET /nfts/{nftId}/favorites?dateFavorited={dateFavorited}&sortBy={name}
    A->>R: queries favorite table for all records with the given favorited date leverages join to use ORDER_BY operation on sort_by criteria
    R->>A: return matching favorite records ordered by sort_by criteria
    A->>C: respond with 200 and ordered array of favorite records
    C->>User: display search and sort results
```

## Fetch List of Favorites
```mermaid
sequenceDiagram
    actor User
    participant C as Client
    participant A as API
    participant R as RDS
    User->>C: navigates to favorites list
    C->>A: GET /users/{userId}/favorites
    A->>R: queries favorites table for all favorites associated with user
    R->>A: return found records
    A->>C: respond with 200 and user's favorite records
    C->>User: display user's favorites along with image
```

## Fetch Changelog
```mermaid
sequenceDiagram
    actor User
    participant C as Client
    participant A as API
    participant R as RDS
    User->>C: navigates to history for an NFT
    C->>A: GET /nft/{nftId}/changelog
    A->>R: queries changelog table for all records associated with given nft_id
    R->>A: return found changelog records
    A->>C: respond with 200 and array of changelog records
    C->>User: display changelog records
```

